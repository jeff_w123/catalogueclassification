## A Mean.js implementation of a web app that stores/edits products and categories.

<h3>Install Requirements:</h3>
<br>You will need to have the following installed on your computer:
<br>- Node 8 (Do not install a later version, or the stack will not run)
<br>- MongoDB: This should be running and listening on default port 27017

<h3>To Run App:</h3>
<br>In the project folder run the following commands:
<br>- npm install
<br>- npm seed
<br>- npm run

<br>App will be up and running at:
<br>http://localhost:3000

<h3>Note:</h3>
<br>Mean.js is no longer maintained.
